# devops_exercise

## Description

Provision a `centos7` box, via Ansible, to serve a Django application using NGINX and gunicorn.

## Instructions

Fork this project. Open a merge request when finished.

All tasks should be added to `site.yml`. The following steps must be completed:

1. The application should live in the `/var/www/django_exercise` directory.
2. Create a log directory for the application at `/var/logs/django_exercise`.
3. Setup `gunicorn` to serve the Django project via unix socket. 
4. Create a `systemd` unit that is able to start/stop/restart the `gunicorn` process.
5. NGINX should have a server block defined to serve the Django project via proxy to the unix socket.
